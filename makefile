SHELL := /bin/bash
PY=python
PANDOC=pandoc

BASEDIR=$(CURDIR)

SOURCEFILE=`ls _*.md`
SOURCENAME=`ls _*.md | cut -d'.' -f1 |  tail -c +2`

default: all
all: csv pdf
.PHONY: csv pdf all 

pdf:
	@echo 'making a pdf'
	pandoc  "$(BASEDIR)"/$(SOURCEFILE) --filter pandoc-placetable -o "$(BASEDIR)/$(SOURCENAME).pdf" 


	
csv:
	@echo 'exporting CSV from XLSX'
	/bin/bash "$(BASEDIR)"/scripts/convertxlsx.sh
