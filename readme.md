
### dependency (macOS)

#### brew

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

#### haskell
brew install ghc cabal-install


#### pandoc

cabal update &&  cabal install pandoc 


#### latex

```
brew tap caskroom/cask
brew cask install mactex
```

#### svg

```
brew install librsvg

```

#### [pandoc-placetable](https://github.com/baig/pandoc-csv2table)

```
cabal install -f inlineMarkdown pandoc-placetable
```

####  pythonPIP
```
brew install python
```

#### [xlsx2csv](https://github.com/dilshod/xlsx2csv)

```  
pip install xlsx2csv
```



