#!/bin/bash
# get the absoluth path
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

cd $SCRIPTPATH
FILES=$SCRIPTPATH/../xlsx/*xlsx

for f in $FILES
do

echo "converting $f to csv"
xlsx2csv --all $f ../csv/

done

#xlsx2csv --all ../xlsx/budget.xlsx ../csv/
