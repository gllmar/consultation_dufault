---
geometry: margin=3.5cm
papersize: letter
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
	\lhead{\textbf{Consultation technologique}}
	\chead{}
	\rhead{\today}
	\lfoot{\MYhref{mailto:gllm@artificiel.org}{gllm@artificiel.org}}
	\cfoot{\thepage}
	\rfoot{\MYhref{https://gllmar.github.io}{gllmar.gitlab.io}}
	\renewcommand{\headrulewidth}{.5pt}
	\renewcommand{\footrulewidth}{.5pt}
	\usepackage{svg}
    \usepackage{arev}
	\usepackage[T1]{fontenc}
	\usepackage[french]{babel}
	\selectlanguage{french}
    \newcommand{\MYhref}[3][darkgray]{\href{#2}{\color{#1}{#3}}} 
---

\tableofcontents
\pagebreak



# Projet flamenco

## Contexte

Concevoir deux objets mobiles sur scène dotés chacun d'une lumière contrôlable (on/off alterné et position horizontal/vertical). 
L'idée est d'éclairer une performeuse de différent point de vue mobile afin de créer des ombres riches et expressifs. 

## Spécifications

* Structure mobile sur scène, 
	* Autonome / controllable
	* Sans Fil 
	* Rechargeable 
	* Circonscription dans une zone de jeu
	* Supporte une lumière orientable 

* Lumière orientable
	* Puissance
	* Orientation contrôlable
	* Controle on/off lumière

\pagebreak

## Structure mobile sur scène 

### Roomba Create 2

La proposition recommandée est d'utiliser robot de type *Roomba*, plus spécifiquement le modèle *Roomba Create 2* qui est destiné à la communauté de la robotique. 

![Roomba create 2](svg/roomba.svg)

Cette plateforme de développement permet de bouger un objet sur une surface plane. 
Viens avec son kit de batterie ainsi que sa borne de chargement.
Viens avec deux programmes, soit clean et spot.
À un port sériel qui permet d'envoyer des commandes au robot
À une bonne documentation et une communauté qui a été assez active pour laisser des traces
est bon marché considérant le nombre de capteurs, d'intelligence et la qualité de manufacture.
Le Roomba create 2 vient : 

* borne de recharge, 
* un fil sériel->USB 
* une batterie NiCD
* Une unité Roomba Create 2

```{.table file="csv/t_roomba.csv" header=yes caption="roomba create 2"}
```

\pagebreak

### Batteries :

Les batteries (NiCD) fournies avec le Create 2 offrent une autonomie relative de 3 heures et doivent être rechargées pendant 7 heures. Cette chimie de batterie a le désavantage d'offrir très peu de cycle de charge (~300), peu de densité d'énergie par rapport à son poids en comparaison avec des batteries de type lithium-ion [^batterie].
  
[^batterie]: [comparaison de chimies de batteries](http://batteryuniversity.com/learn/archive/whats_the_best_battery)

Des batteries de remplacements pour Roomba sont disponibles sur le marché et vaudraient la peine d'être achetées.

```{.table file="csv/t_batteries.csv" header=yes caption="batteries lithium"}
```



### Zone de jeu

Il est possible de circonscrire la zone de jeu en utilisant des barrières infrarouges.


![Barrière infrarouge](img/roomba_ir_barrier.jpg){width=50%}

[dual-mode-virtual-wall-barrier](https://store.irobot.com/default/parts-and-accessories/roomba-accessories/900-series/dual-mode-virtual-wall-barrier/4469425.html)

La zone de restriction maximum est de 8 pieds par borne. Au nombre de 8, il serait possible de circonscrire une surface de jeu de 16 pieds par côté.

```{.table file="csv/t_zones.csv" header=yes caption="Zone de jeu"}
```

\pagebreak

### Contrôle du comportement

Dans l'intention artistique transmise, il y a une volonté de travailler avec la dynamique un peu aléatoire native au robot. 

Le robot dispose d'un programme interne destiné à couvrir l'ensemble de la surface à l'intérieur du périmètre qui lui est accessible. 

Déclencher ce comportement de manière conditionnelle implique d'embarquer un système pouvant communiquer avec le protocole sériel du Roomba [^documentation_raspberry]. Le chemin qui semble le plus logique est d'utiliser un raspberry pi ainsi que le câble USB->SERIAL  (fourni avec le create2). 

Un exemple de contrôle conditionnel pourrait s'exprimer comme suit : 

```
Lecture périodique de l'état du roomba 
Si batterie basse -> chercher DOCK
Si batterie rechargée -> activer programme Clean   
```

Plus on cherche à contrôler précisément le Roomba, plus le temps/coût de programmation augmente. Cette relation proportionnelle au nombre de fonctions que le contexte nécessite pourrait être influencée par des besoins artistiques. L'évaluation suivante couvre le minimum pour interagir avec le robot et se familiariser avec la programmation de fonction de base.

```{.table file="csv/t_ctl_roomba.csv" header=yes caption="Programmation Roomba"}
```


 [^documentation_raspberry]:[documentation minicom raspberry ](https://www.irobotweb.com/-/media/MainSite/PDFs/About/STEM/Create/RaspberryPi_Tutorial.pdf?la=en)

\pagebreak



## Lumière orientable

L'objectif ici et concevoir une lumière au contrôle asservissable sur trois axes.
La solution recommandée est l'utilisation d'un stabilisateur de caméra de type gimbale surmonté d'une lampe DEL interrompue par un relais.

### Stabilisateur de caméra(gimbale)

Dans le processus de sélection d'un produit incluant les critères suivants.

*  Doté un passthrought AV de manière à mettre la lumière sur cette ligne 

*  Favoriser un modèle low profile vs ceux qui se tiennent à la main

*  Disposant d'une manette physique sans fil 

*  Doté d'un mode Follow qui préserve l'orientation vers le fond de scène

* Le plus silencieux possible tout en étant rapide

En considérant ces facteurs, le candidat couvrant le mieux les critères est le Zhinyun Rider-M
![Zhinyun Rider-M](svg/rider_m.svg)




### Télécommande

L'utilisation de la télécommande prévu pour contrôler le stabilisateur est la proposition recommandé. Deux télécommandes peuvent être individuellement pairé à un stabilisateur via bluetooth, la porté maximale recommandé est de 10 mètres dans une zone exempt de pollution radio (2.4 ghz).

Il est encore inconnu s'il est possible, mais il serait pratique d'utiliser le bouton enregistrement pour faire basculer l'état de la lumière.

\pagebreak

### Modes d'opération


![interface de contrôle](svg/remote.svg) 

en mode installation/exposition, il serait envisageable d'utiliser les programmes intégrés de stabilisation. 

Si ce mode est insuffisant pour les besoins artistiques, il est aussi envisagé de produire un microcontrôleur qui opère directement sur le voltage du Joystick de la télécommande. Ce segment de travail n'est pas présentement évalué, il est un projet autonome.   


### Alimentation électrique

Le stabilisateur fonctionne avec des batteries 18350, le chargeur vient à part. 

Il à été demandé d'évaluer la faisabilité de permettre  au stabilisateur d'opérer à partir de la batterie du Roomba via un convertisseur DC-DC.

```{.table file="csv/t_hack_batterie.csv" header=yes caption="Conversion 12v -> 7.4v"}
```

\pagebreak

### Lumière

Utiliser le passe-thru AV pour alimenter une située au lieu d'une caméra sur le stabilisateur. Favoriser un modèle 12v mono source (minimiser les duplicata d'ombre), la plus puissante possible (10 Watts et plus). 

![lumière](img/lx_dim.jpg){width=50%}


Cette lumière est disponible avec deux angles de diffusion soit Spot et Flood. Probablement que Flood est préférable à Spot question d'avoir une ombre générale 

![Flood vs spot][img/lx_flood_spot.jpg]{width=50%}

Utiliser le 12v de la Roomba pour éviter d'avoir une batterie de plus à gérer. Utiliser un relais DC monté sur le raspberry pi afin de minimiser le filage flotant.


```{.table file="csv/t_lx_materiel.csv" header=yes caption="matériel lumière"}
```

Créer un logiciel de type serveur qui permet de dialoguer avec le relais ainsi qu'une interface de contrôle OSC. 

```{.table file="csv/t_lx_prog.csv" header=yes caption="Programmation Lumière"}
```

\pagebreak

## Budget 

```{.table file="csv/t_robot_lx.csv" header=yes caption="Détail robotique"}
```
```{.table file="csv/t_robot_total.csv" header=yes caption="Estimé Projet"}
```

\pagebreak

# *Strange moods and dissonant feelings*, suite

##  Contexte

Ce projet fait usage d'une caméra vidéo SDI ( Marshal Electronic) et d'un délai vidéo HD de marque Gra-Vue. La caméra filme sa sortie projetée depuis un projecteur créant un écho vidéo contrôlable modulé par une performance entre la projection et la caméra. La mise à jour technologique de ce projet consiste à interfacer de manière digitale les paramètres du matériel ainsi qu'à rajouter certaines fonctionnalités notamment faire tourner la caméra sur l'axe optique. 


## Spécifications
* Doter le projet d'un contrôle continu sur l'axe (X; Roll/Rouleau)
* permettre zoom in/out très lent opérable manuellement (mode performance) et automatiquement (mode installation)
* créer une interface de contrôle permettant de positionner un nombre arbitraire de points paramétrés ( distance de focus, valeur de zoom et vitesse de rotation) et passer d'un à l'autre en un temps définissable. 
* Pouvoir intervenir de manière "élastique" sur les paramètres via une interface de contrôle.  
* Pouvoir avoir un mode installation où les paramètres fluctuent de manière relativement programmée.
* [à confirmer] créer un système rs-232 permettant de contrôler le nombre d'images de vidéo délais.  

\pagebreak

## Contrôle optique de lentille CS

La lentille actuelle a le zoom et le focus sur deux contrôles différents. Plus spécifiquement, le plan focal bouge si on change le zoom ce qui implique de devoir changer les deux paramètres en même temps. Aussi, les contrôles sur la lentille actuelle sont très petits et sensibles. Les contrôles actuels sont *Set and Forget* au lieu d'être aisément manipulable en performance.    

### Lentille motorisée 

Avoir recours à une lentille motorisée compatible avec la caméra actuelle et de crée ou d'avoir recours à une interface externe pour contrôler les paramètres de zoom et focus. 

Candidat lentille mount CS

![MPL2.8-8.5MPI ](img/mpl_motorize.jpg)

| Part Number  | Focal Length   | Optical Format | Mount | Aperture | Iris Range | IR Corrected | Dim 		| Weight|
|-			   |-				|-				 |-		 |-			|-			|-				|-			|-		|
|MPL2.8-8.5MPI |2.8–8.5mm       |1/3" 			 |CS 	 |F1.2 		|P-iris 	|Yes 			| ø 57 (2.24") x 31.65mm  (1.24") | 73.4g (0.16lbs)|
Table:Spécifications MPL2.8-8.5MPI

* référence produit : [https://www.arecontvision.com/product/Lenses/MPL2.8-8.5MPI#Description](https://www.arecontvision.com/product/Lenses/MPL2.8-8.5MPI#Description)

### Contrôle paramètre lentille CS

Un objets pour faire le relais entre un protocole de communication et le protocole électrique des moteurs de la lentille est nécessaire pour faire dialoguer l'ensemble. 
Le pin out de contrôle optique de la lentille semble être standardisé (très utilisé dans le domaine de la surveillance). 
La proposition recommandée est d'aller vers une plateforme de développement existante pour contrôler la lentille soit : 

![KuroKesu Zoom Lens Controller Developpement Kit](img/f_zoom_devkit.jpg)


### Micro-logiciel de contrôle

Cette plateforme de développement est basée compatible avec l'environnement Arduino ce qui rend la modification du micrologiciel possible si ce dernier ne répond pas spécifiquement aux besoins artistiques.

Le code de la plateforme ainsi qu'un exemple de protocole d'échange est disponible à l'URL suivant :

* [https://github.com/Kurokesu/motorized_zoom_lens](https://github.com/Kurokesu/motorized_zoom_lens)

À noter que ce dispositif doit être branché via USB dans un micro-ordinateur pour être contrôlé via protocole sériel (USB). 
Le fil USB reliant le micro-ordinateur et le micro contrôleur devra être étendu à travers le slipring (4 conducteurs).

### Implémentation logicielle:
* Contrôler Zoom et Focus via une interface
* Sauvegarder des états de position Zoom/focus
* Interpoler entre les deux positions sauvegardées
* Définir le temps entre l'interpolation via une interface 
* Mode automatique pour installation sans opération directe
* Possibilité d'Override manuel en mode élastique

Créer un mode exposition activable avec un bouton par exemple qui passe entre les presets

### Pitfall

* La lentille ne pourrait pas être compatible avec la caméra actuelle ?
* Le pin out du contrôleur pourrait être différent de celui de la lentille
* les steps de la lentille pourraient être trop visible pour un usage scénique 
* interface physique? Interface software? 

### Budget : contrôle optique

```{.table file="csv/t_lentille.csv" header=yes caption="Estimé control optique"}
```

\pagebreak

## Caméra en rotation continue

### Objectifs :

* Faire tourner une caméra sur l'axe rotation verticale de manière continue 
* avoir une interface absolue  qui permet de contrôler la vitesse de rotation.
* Passer le courant à la caméra
* Intégrer le module slipring BNC afin de permettre la circulation d'électricité et de signal HD

![slipring BNC](img/slip_ring_bnc.jpg)


\pagebreak

### Hollow Rotary Stepper

#### Caractéristiques:

* Super heavy-duty
* Doté d'une courroie de réduction 6/1
* Compatible avec un écosystème de stepper motor (nema 23?)
	* dans l'éventualité que celui fourni ne dispose pas de la vitesse adéquate, d'autres moteurs similaires peuvent aisément le remplacer 
* sépare le problème de sourcer un moteur disposant d'un trou pour passer le filage et de suffisamment de torques
* relativement simples à contrôler depuis un arduino

![Hollow Rotary Stepper](img/hollow_rotary_stepper.jpg){width=60%}

\pagebreak

### Contrôle de rotation 

Utiliser le même Raspberry Pi déjà utilisé pour ce projet avec un *Stepper Motor HAT * pour contrôler le stepper motor.
 
![Hollow Rotary Stepper](img/raspberry_pi_stepper.jpg){width=50%} 

Prévoir deux alimentations électriques indépendantes (5v et 12v)

Créer un logiciel permettant de contrôler la vitesse de rotation du moteur stepper depuis une information OSC


#### Fixation et ajustements caméra 

L'idée ici est de permettre un ajustement précis pour centrer l'optique de la caméra.

Une recommandation pourrait être d'utiliser 2 modules de rig DSLR pour fixer puis pour ajuster le centre de la caméra sur l'axe de rotation

![système de fixation](svg/rig.svg)


\pagebreak

### Intégration

Voici une proposition d'intégration à réaliser. 
L'idée est que la caméra et le contrôle optique sont sur la partie mobile (en rotation) alors que le micro-ordinateur est sur la partie fixe. 

![prototype d'intégration](svg/slipring_cnc.svg)


\pagebreak

### Budget : rotation continue

```{.table file="csv/t_rotation.csv" header=yes caption="Estimé rotation"}
```

## Total : Projet

```{.table file="csv/t_strange.csv" header=yes caption="Estimé projet Strange"}
```

\pagebreak

# Gestion de projet et interstices

## Interface de contrôle globale

Il a été proposé d'avoir recours à une interface virtuelle afin de minimiser les coûts de développement d'interface physique. Un iPad de format régulier serait à prévoir ainsi que des adaptateurs pour le rendre filaire. Une borne wifi dédiée au projet sera aussi à inclure dans ce lot.

## Gestion de projet, recherche et consultation
Étant donné la nature communicante des projets étudiés, un forfait global est englobe la recherche appliquée aux deux projets.

## Résidence à Québec (RV) deux jours
Afin de ficeler l'intégration finale tout deux jours de résidence sont prévus au mois de novembre 2018 cette date constitue le milestone final et le dernier livrable de ce projet.

```{.table file="csv/t_meta.csv" header=yes caption="Budget interstices"}
```

\pagebreak

# Total estimé pour l’ensemble

```{.table file="csv/t_total.csv" header=yes caption="Estimé projet Strange"}
```

